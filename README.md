# Plataformal Solution
## _Dogia_
Proyecto para solucionar prueba técnica usando django
para la construcción de un servicio API Rest que
permita gestionar una agenda de eventos

## Requisitos y entorno

- Windows 10 Pro comp. 18363.1440
- Python 3.9.2
- pip 21.0.1
- Django 3.2
- psycopg2
- PostgreSQL 13
- pgAdmin 4 v5.1

## Ejecución

> Cree las migraciones mediante python manage.py makemigrations
> Ejecute las migraciones mediante python manage.py migrate
> Ejecute el servidor de desarrollo con python manage.py runserver

#Rutas
*Clientes*
> http://localhost:8000/api/v1/ListarClientes
> http://localhost:8000/api/v1/ListarClientes/#id
> http://localhost:8000/api/v1/CrearCliente
> http://localhost:8000/api/v1/ActualizarCliente/#id
*Eventos*
> http://localhost:8000/api/v1/ListarEventos
> http://localhost:8000/api/v1/ListarEventos/#id
> http://localhost:8000/api/v1/CrearEvento
> http://localhost:8000/api/v1/ActualizarEvento/#id

# Atencion !!!
El servidor es de prueba, para temas de agilidad en el desarrollo de API se ha desbloqueado la necesidad de login y la validación del método,
por lo tanto, cualquier solicitud sobre los modelos de la base de datos será procesada, si desea implementar la cookie
CSRF para usarla en producción debe crear el algoritmo para asignar la cookie con un login.
Jamás exponga esta web a producción sin esa implementación ya que no se validará la autorización para realizar
cambios en los modelos
