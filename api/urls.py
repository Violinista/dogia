from api.routes.eventos import create_evento, get_evento, get_eventos, update_evento
from api.routes.clientes import create_cliente, get_clientes, get_cliente, update_cliente
from django.urls import path

from . import views

urlpatterns = [
    #Clientes
    path('ListarClientes', get_clientes, name='get_clientes'),
    path('ListarClientes/<int:id>', get_cliente, name='get_cliente'),
    path('CrearCliente', create_cliente, name='create_client'),
    path('ActualizarCliente/<int:id>', update_cliente, name='update_client'),
    #Eventos
    path('ListarEventos', get_eventos, name='get_eventos'),
    path('ListarEventos/<int:id>', get_evento, name='get_evento'),
    path('CrearEvento', create_evento, name='create_evento'),
    path('ActualizarEvento/<int:id>', update_evento)
]