from django.contrib import admin
from .models import Cliente, Evento
# Register your models here.

admin.site.register(Cliente)
admin.site.register(Evento)
