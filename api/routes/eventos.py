from api.models import Evento
from django.http import JsonResponse
import json

def get_eventos(request):
    eventos = Evento.objects.all()
    response = {
        "method": request.method,
        "eventos": []
    }
    for evento in eventos:
        response["eventos"].append(evento.Json())
    return JsonResponse(response)

def get_evento(request, id):
    evento = Evento.objects.get(id=id)
    response = {
        "method": request.method,
        "evento": evento.Json()
    }
    return JsonResponse(response)


def create_evento(request):
    try:
        body = json.loads(request.body)
        evento = Evento(
                fecha = body["identificacion"],
                asistentes = body["nombres"],
                motivo = body["apellidos"],
                cliente_id = body["telefono"]
            )
        evento.save()
        response = {
            "method": request.method,
            "cliente": evento.Json()
        }
        JsonResponse.status_code = 201
        return JsonResponse(response)
    except:
        JsonResponse.status_code = 500
        return JsonResponse({"method": request.method, "evento": {}, "error": "Error al crear el recurso, probablemente atributos incompletos"})

def update_evento(request, id):
    try:
        body = json.loads(request.body)
        evento = Evento.objects.get(id=id)
        for attr in body:
            evento.__setattr__(attr, body[attr])
        evento.save()
        response = {
            "method": request.method,
            "cliente": evento.Json()
        }
        return JsonResponse(response)
    except:
        response = {
            "method": request.method,
            "evento": evento.Json(),
            "error": "No se ha podido actualizar el cliente probablemente por un error interno del servidor"
        }
        return JsonResponse(response)