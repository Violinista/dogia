from api.models import Cliente
from django.http import JsonResponse
import json

# Create your views here.
def get_clientes(request):
    clientes = Cliente.objects.all()
    response = {
        "method": request.method,
        "clientes": []
    }
    for cliente in clientes.iterator():
        response["clientes"].append(cliente.Json())
    return JsonResponse(response)

def get_cliente(request, id):
    cliente = Cliente.objects.get(id=id)
    response = {
        "method": request.method,
        "cliente": cliente.Json()
    }
    return JsonResponse(response)

def create_cliente(request):
    try:
        body = json.loads(request.body)
        cliente = Cliente(
                identificacion = body["identificacion"],
                nombres = body["nombres"],
                apellidos = body["apellidos"],
                telefono = body["telefono"],
                email = body["email"],
                departamento = body["departamento"],
                ciudad = body["ciudad"],
                edad = body["edad"]
            )
        cliente.save()
        response = {
            "method": request.method,
            "cliente": cliente.Json()
        }
        JsonResponse.status_code = 201
        return JsonResponse(response)
    except:
        JsonResponse.status_code = 500
        return JsonResponse({"method": request.method, "cliente": {}, "error": "Error al crear el recurso, probablemente atributos incompletos"})

def update_cliente(request, id):
    try:
        body = json.loads(request.body)
        cliente = Cliente.objects.get(id=id)
        for attr in body:
            cliente.__setattr__(attr, body[attr])
        cliente.save()
        response = {
            "method": request.method,
            "cliente": cliente.Json()
        }
        return JsonResponse(response)
    except:
        response = {
            "method": request.method,
            "cliente": cliente.Json(),
            "error": "No se ha podido actualizar el cliente probablemente por un error interno del servidor"
        }
        return JsonResponse(response)