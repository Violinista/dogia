from django.db import models
from django.db.models.fields import IntegerField

# Create your models here.
class Cliente(models.Model):
    identificacion = models.CharField(max_length=256)
    nombres = models.CharField(max_length=256)
    apellidos = models.CharField(max_length=256)
    telefono = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    departamento = models.CharField(max_length=256)
    ciudad = models.CharField(max_length=256)
    edad = models.IntegerField()
    def Json(self):
        return {
            "id": self.id,
            "identificacion": self.identificacion,
            "nombres": self.nombres,
            "apellidos": self.apellidos,
            "telefono": self.telefono,
            "email": self.email,
            "departamento": self.departamento,
            "ciudad": self.ciudad,
            "edad": self.edad
        }
class Evento(models.Model):
    fecha = models.DateTimeField()
    asistentes = models.IntegerField()
    motivo = models.CharField(max_length=256)
    cliente_id = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    def Json(self):
        return {
            "id": self.id,
            "fecha": self.fecha,
            "asistentes": self.asistentes,
            "motivo": self.motivo,
            "cliente_id": self.cliente_id,
        }
